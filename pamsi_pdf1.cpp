#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include<ctime>
#include<fstream>
#include<string>
#include<vector>
#define SIZE 5


using namespace std;

class Shellsort
{
    vector <int> data_array;
public:

    Shellsort() {};


    Shellsort(vector <int> data_array);

    void sorting();

    vector <int> get() { return data_array; }
};

Shellsort::Shellsort(vector <int> data_array)
{

    this->data_array = data_array;
}
void Shellsort::sorting()
{
    int translation;

    for (translation = 1; translation < data_array.size(); translation = 3 * translation + 1);
    translation /= 9;
    if (!translation) translation++;

    int i, p;

    while (translation)
    {
        for (int j = data_array.size() - translation - 1; j >= 0; j--)
        {
            p = data_array[j];
            i = j + translation;
            while ((i < data_array.size()) && (p > data_array[i]))
            {
                data_array[i - translation] = data_array[i];
                i += translation;
            }
            data_array[i - translation] = p;
        }
        translation /= 3;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Merge_sort
{
    vector <int> data_array;
    vector <int> auxiliary_array;
public:
    Merge_sort() {};
    Merge_sort(vector <int> data_array);
    void sorting(int left, int right);
    vector <int> get() { return data_array; }
};

Merge_sort::Merge_sort(vector <int> data_array)
{
    this->data_array = data_array;
    auxiliary_array.resize(data_array.size());
}
void Merge_sort::sorting(int left, int right)
{
    int center, i, i1, i2;
    center = (right + left + 1) / 2;
    if (center - left > 1) sorting(left, center - 1);
    if (right - center > 0) sorting(center, right - 1);

    i1 = left;
    i2 = center;
    for (i = left; i <= right; i++)
    {
        if ((i1 == center) || ((i2 <= right) && (data_array[i1] > data_array[i2])))
        {
            auxiliary_array[i] = data_array[i2++];
        }
        else
        {
            auxiliary_array[i] = data_array[i1++];
        }
    }
    for (i = left; i <= right; i++)
    {
        data_array[i] = auxiliary_array[i];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Quick_sort
{
    vector <int> data_array;
public:

    /* Konstruktor bezparametryczny */
    Quick_sort() {};

    /* Konstrukor, który nada wartość naszej tablicy z danymi */
    Quick_sort(vector <int> data_array);


    vector <int> get() { return data_array; }
    void sorting(int left, int right);
    void Sort_reverse(int left, int right);
};

Quick_sort::Quick_sort(vector <int> data_array)
{
    /* this wchodzi do atrybtów klasy Quick_sort i przypisuje zmiennej
    data_array wartość, która podaliśmy w argumncie tej funkcji.*/
    this->data_array = data_array;
}
void Quick_sort::sorting(int left, int right)
{
    int i = (left + right) / 2;
    int pivot = data_array[i];
    data_array[i] = data_array[right];
    int j = left;
    for (i = left; i < right; i++)
    {
        if (data_array[i] < pivot)
        {
            swap(data_array[i], data_array[j]);
            j++;
        }
    }
    data_array[right] = data_array[j];
    data_array[j] = pivot;
    if (left < j - 1)
        sorting(left, j - 1);
    if (right > j + 1)
        sorting(j + 1, right);
}

void Quick_sort::Sort_reverse(int left, int right)
{
    int i = (left + right) / 2;
    int pivot = data_array[i];
    data_array[i] = data_array[right];
    int j = left;
    for (i = left; i < right; i++)
    {
        if (data_array[i] > pivot)
        {
            swap(data_array[i], data_array[j]);
            j++;
        }
    }
    data_array[right] = data_array[j];
    data_array[j] = pivot;
    if (left < j - 1)
        sorting(left, j - 1);
    if (right > j + 1)
        sorting(j + 1, right);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void wczytaj(vector < vector< vector <int>>>& data_array, char mode)
{
    fstream file;
     
    int number_of_set;
    
    if (mode == 'g')
    {
        string number_sets[] = { "10k","50k" ,"100k" ,"500k","1m" };
        int multiplier[] = { 10,50 ,100 ,500,1000 };

        /* Funkcja resize(n) zmienia rozmiar wektora. Gdy rozmiar Wektora > n to dodatkowe
        elementy są niszczone.                                                  */

        /* Ilość kontenerów do posortowania 10k, 50k,... */
        data_array.resize(SIZE);

        for (int i = 0; i < SIZE; i++)
        {
            /* ios::in otwieramy plik do odczytu danych. */
            file.open("data" + number_sets[i] + ".txt", ios::in);

            if (!file.is_open())
            {
                cout << "Błąd! Nie można otworzyć pliku." << endl;
                exit;
            }

            /* ilość zestawów w poszczególnych kontenerach -> 100 tablic w każdym */
            data_array[i].resize(100);

            for (int k = 0; k < 100; k++)
            {
                file >> number_of_set;
            
                /* ilość liczb w każdym zestawie */
                data_array[i][k].resize(1000 * multiplier[i]);

                for (int j = 0; j < 1000 * multiplier[i]; j++)
                {
                    file >> data_array[i][k][j];
                }
            }
            file.close();
        }
    }
    else if (mode == 't')
    {
        file.open("test_data.txt", ios::in);
        data_array.resize(1);
        data_array[0].resize(100);
        for (int k = 0; k < 100; k++)
        {
            file >> number_of_set;

            data_array[0][k].resize(10);

            for (int j = 0; j < 10; j++)
            {
                file >> data_array[0][k][j];
            }
        }
    }
}

bool test(vector<int> array)
{
    for (int i = 0; i < array.size() - 1; i++)
    {
        if (array[i] > array[i + 1])
        {
            return false;
        }

    }


    return true;
}

void Generation(char mode)
{
    fstream file;
    if (mode == 'g')
    {
        string number_sets[] = { "10k","50k" ,"100k" ,"500k","1m" };
        int multiplier[] = { 10,50 ,100 ,500,1000 };
        for (int i = 0; i < SIZE; i++)
        {
            /* ios::out -> otwarcie pliku do zapisu */
            file.open("data" + number_sets[i] + ".txt", ios::out);

            /* tworzymy 100 tablic, każda ma 1000*multiplier[i] elementów */
            for (int k = 0; k < 100; k++)
            {
                file << endl << k << endl;
                for (int j = 0; j < 1000 * multiplier[i]; j++)
                {
                    file << rand() << " ";
                }
            }
            file.close();
        }
    }
    else if (mode == 't')
    {
        file.open("test_data.txt", ios::out);
        for (int k = 0; k < 100; k++)
        {
            file << endl << k << endl;
            for (int j = 0; j < 10; j++)
            {
                file << rand() << " ";
            }
        }
    }


    file.close();

}

void Display(vector<int> array)
{
    for (int i = 0; i < array.size(); i++)
    {
        cout << array[i] << " ";
    }
}







int main()
{

    /* Przed wywołaniem funkcji rand() konieczne jest wywołanie funkcji srand().
    Argumentem funkcji srand jest tak zwane ziarno, które inicjuje generator wartości pseudolosowych.
    Dla każdego ziarnia sekwencja liczb pseudolosowych jest inna, dlatego też powinniśmy zadbać o to
    aby przy każdym wywołaniu funkcji rand() ziarno było inne.

    Funkcja time(NULL) zwraca aktualny czas procesora, co rozwiązuje powyższy problem.       */

    srand(time(NULL));


    /* Generacja daty do pliku results*/
    time_t czas;
    struct tm* ptr;
    time(&czas);
    ptr = localtime(&czas);
    char* data = asctime(ptr);

    /* Zmienna plikowa */
    fstream file;

    /* Trójwymiarowa tablica danych */
    vector < vector< vector <int>>> data_array;
    vector <int>auxiliary_array;

    Generation('g');

    /* clock_t - Typ do przechowywania liczby cykli zegara, Niezbędnik przy użyciu
    funkcji clock() */

    clock_t distance = 0;

    wczytaj(data_array, 'g');
    file.open("results.txt", ios::app);
    /* Wypisuje datę do pliku z wynikami */
    file << data << endl;
    char sign;
    double procent;
    while (true)
    {
        distance = 0;
        cout << "W jaki sposób mam sortować? sortowanie Shella - 'h', sortowanie przez scalanie - 's', sortowanie szybkie - 'q'" << endl;
        cin >> sign;
        cout << "W jakiej części posortować tablicę przed pomiarem czasu? Podaj wartość procentową (0-100), uwaga! Wartość 100 jest równoważna z wykonaniem sortowania odwrotnego.";
        cin >> procent;
        switch (sign)
        {
        case 'h':
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        Quick_sort sort(data_array[i][j]);
                        sort.sorting(0, (data_array[i][j].size() * procent / 100) - 1);
                        auxiliary_array = sort.get();
                    }
                    else if (procent == 100)
                    {
                        Quick_sort sort(data_array[i][j]);
                        sort.Sort_reverse(0, (data_array[i][j].size()) - 1);
                        auxiliary_array = sort.get();
                    }
                    else
                    {
                        auxiliary_array = data_array[i][j];
                    }
                    Shellsort sort(auxiliary_array);
                    clock_t start = clock();
                    sort.sorting();
                    distance += clock() - start;
                }

                file << distance / 100 << endl;
            }
            break;
        case 's':
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        Quick_sort sort(data_array[i][j]);
                        sort.sorting(0, (data_array[i][j].size() * procent / 100) - 1);
                        auxiliary_array = sort.get();
                    }
                    else if (procent == 100)
                    {
                        Quick_sort sort(data_array[i][j]);
                        sort.Sort_reverse(0, (data_array[i][j].size()) - 1);
                        auxiliary_array = sort.get();
                    }
                    else
                    {
                        auxiliary_array = data_array[i][j];
                    }
                    Merge_sort sort(auxiliary_array);
                    clock_t start = clock();
                    sort.sorting(0, auxiliary_array.size() - 1);
                    distance += clock() - start;
                }

                file << distance / 100 << endl;
            }
            break;
       
        case 'q':

            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (procent > 0 && procent != 100)
                    {
                        /* Tworzymy obiekt klasy sortowowanie_szybkie */
                        Quick_sort sort(data_array[i][j]);

                        /* Weź po uwagę tylko pierwsze x procent elementów i posortuj*/
                        sort.sorting(0, (data_array[i][j].size() * procent / 100) - 1);

                        /* Do tablicy pomocniczej przypisz częsciowo posortowaną już tablicę.*/
                        auxiliary_array = sort.get();
                    }
                    else if (procent == 100)
                    {
                        Quick_sort sort(data_array[i][j]);
                        sort.Sort_reverse(0, (data_array[i][j].size()) - 1);
                        auxiliary_array = sort.get();
                    }
                    else
                    {
                        auxiliary_array = data_array[i][j];
                    }


                    Quick_sort sort(auxiliary_array);

                    /* Liczymy czas każdego sortowania */
                    clock_t start = clock();

                    /* Sortujemy tabicę pomocniczą czyli tablicę z częściowo posortowanymi już danymi*/
                    sort.sorting(0, auxiliary_array.size() - 1);

                    distance += clock() - start;
                }
                /* Jak posortujesz już wszystkie 100 tablic to średni czas wypisz do pliku results*/

                file << distance / 100 << endl;
            }
            break;
        default:
            break;
            }
        }

        file.close();





    }


